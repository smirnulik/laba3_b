/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans_b;

import entity_b.Contact;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author пк
 */
@Stateless
public class ContactFacade extends AbstractFacade<Contact> {
    @PersistenceContext(unitName = "laba3_bPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ContactFacade() {
        super(Contact.class);
    }
    
    public Integer createContact(Contact contact)
    {
        return create(contact);
    }
    
    public Contact rollbackExeption(Contact contact)
    {
        create(contact);
        throw new EJBException();
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void createContactOnlyDB2(Contact contact)
    {
        create(contact);
    }
}
